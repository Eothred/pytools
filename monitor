#!/usr/bin/env python
"""
Monitors a set of files for change, and executes a command when that file
is changed.
"""
# Copyright 2008, 2009 Paul Anton Letnes
  
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
  
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
  
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import optparse
import os
import time
import subprocess

programname = 'monitor'
help_message = '''
    %prog [-h] file1 ... command

This utility monitors a list of files for changes based on their
time stamp, and executes a command when that file is changed. Both
arguments target and command are required. The command argument can be a 
string of commands separated by the ';' character.

Examples:
    %prog mainfile.tex otherfile.tex "pdflatex mainfile"
        Monitor the file mainfile.tex and the file otherfile.tex for changes
        and run pdflatex on mainfile.tex when an update has occurred.
    %prog source.c make
        Monitor source.c for changes and run make when the source file has
        been updated.\
'''

def parse_options(argv):
    'Parse command line arguments and options.'
    parser = optparse.OptionParser(usage=help_message)
    options, arguments = parser.parse_args(argv)
    if len(arguments) < 3:
        # This program requires at least a file to monitor and a command
        # to run, hence, at least 2 arguments. The first argument in the
        # list is the name of the program.
        msg = 'Requires at least 2 arguments!'
        parser.error(msg)
    
    # Arguments: list of files, then command to run
    files = arguments[1:-1]
    command = arguments[-1]
    return files, command

def main(argv=None):
    if argv is None:
        argv = sys.argv
    files, command = parse_options(argv)
    monitor(command, files)

def monitor(command, files):
    '''
    Monitor the files for change based on their modification time,
    and run command if any of them are changed.
    '''
    commands = command.split(';')
    prevtimes = {}
    for f in files:
        prevtimes[f] = 0
    try:
        while True: # Loop until KeyboardInterrupt occurs
            time.sleep(1)
            anyChanged = False
            for f, t in prevtimes.iteritems():
                prevtimes[f], changed = checkIfUpdated(f, t)
                anyChanged = anyChanged or changed
            if anyChanged:
                for cmd in commands:
                    subprocess.call(cmd.split())
    except KeyboardInterrupt:
        print "\n%s was interrupted." % programname

def checkIfUpdated(filename, oldtime):
    'Check file if it has been changed.'
    statinfo = os.stat(filename)
    if statinfo.st_mtime != oldtime:
        return (statinfo.st_mtime, True)
    return (statinfo.st_mtime, False)

if __name__ == "__main__":
    sys.exit(main())






